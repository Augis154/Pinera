#!/bin/bash
mkdir -p build
cd build
cmake -S ../ -B .

if [ -z "$MAKEFLAGS" ]
then
    export MAKEFLAGS=-j$(($(nproc)-2))
fi

make && make shaders && mangohud ./Pinera
cd ..