#version 450

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec2 uv;

layout (location = 0) out vec3 fragColor;
layout (location = 1) out vec3 fragPosWorld;
layout (location = 2) out vec3 fragNormalWorld;

struct PointLight {
    vec4 position;  // ignore w
    vec4 color; // w is intensity
};

layout(set = 0, binding = 0) uniform GlobalUbo {
    mat4 projection;
    mat4 view;
    vec4 ambientLightColor; // w is intensity
    PointLight pointLights[10];
    int lightsActive;
} ubo;

layout (push_constant) uniform PushConstant {
    mat4 modelMatrix;
    mat4 normalMatrix;
} pushConstant;

void main() {
    vec4 positionInWorld = pushConstant.modelMatrix * vec4(position, 1.0);
    gl_Position = ubo.projection * ubo.view * positionInWorld;

    fragNormalWorld = normalize(mat3(pushConstant.normalMatrix) * normal);
    fragPosWorld = positionInWorld.xyz;
    fragColor = color;
}