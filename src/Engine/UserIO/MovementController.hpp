#pragma once

#include "../Window.hpp"
#include "../Object/Object.hpp"

namespace pve {

class MovementController {

public:

    struct KeyMappings {
        SDL_Scancode moveLeft = SDL_SCANCODE_A;
        SDL_Scancode moveRight = SDL_SCANCODE_D;
        SDL_Scancode moveForward = SDL_SCANCODE_W;
        SDL_Scancode moveBackward = SDL_SCANCODE_S;
        SDL_Scancode moveUp = SDL_SCANCODE_SPACE;
        SDL_Scancode moveDown = SDL_SCANCODE_LSHIFT;
        SDL_Scancode lookLeft = SDL_SCANCODE_LEFT;
        SDL_Scancode lookRight = SDL_SCANCODE_RIGHT;
        SDL_Scancode lookUp = SDL_SCANCODE_UP;
        SDL_Scancode lookDown = SDL_SCANCODE_DOWN;

        SDL_Scancode exit = SDL_SCANCODE_ESCAPE;
        SDL_Scancode toggleMouse = SDL_SCANCODE_M;
    };

    MovementController();
    ~MovementController();

    void moveInPlaneXZ(float dt, PveObject& gameObject);

    KeyMappings keys{};
    float moveSpeed{3.0f};
    float lookSpeed{2.0f};
    float mouseSpeed{0.9f};

private:
    bool mouseEnabled = 0;
    glm::vec2 PrevMousePos{};
};
}   // namespace pve