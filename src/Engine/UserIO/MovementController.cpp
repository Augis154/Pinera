#include "MovementController.hpp"

// std headers
#include <limits>
#include <iostream>

namespace pve {

MovementController::MovementController() {
    SDL_ShowCursor(SDL_DISABLE);
    SDL_SetRelativeMouseMode(SDL_TRUE);
    // SDL_ShowCursor(SDL_ENABLE);
    // SDL_SetRelativeMouseMode(SDL_FALSE);
}

MovementController::~MovementController() {}

void MovementController::moveInPlaneXZ(float dt, PveObject& gameObject) {
    const uint8_t* keystate = SDL_GetKeyboardState(NULL);

    // if (keystate[keys.exit]) SDL_QuitEvent();

    int mouseX, mouseY = 0;
    SDL_GetRelativeMouseState(&mouseX, &mouseY);
    // uint32_t mousestate = SDL_GetRelativeMouseState(&mouseX, &mouseY);

    glm::vec3 rotate{0.0f};

    rotate.y += mouseX * mouseSpeed;
    rotate.x -= mouseY * mouseSpeed;

    if (keystate[keys.lookRight]) rotate.y += 1.0f;
    if (keystate[keys.lookLeft]) rotate.y -= 1.0f;
    if (keystate[keys.lookUp]) rotate.x += 1.0f;
    if (keystate[keys.lookDown]) rotate.x -= 1.0f;

    if (glm::dot(rotate, rotate) > std::numeric_limits<float>::epsilon()) {
        // gameObject.transform.rotation += lookSpeed * dt * glm::normalize(rotate);
        gameObject.transform.rotation += lookSpeed * dt * rotate;
    }

    // limit pitch values between about +/- 85ish degrees
    gameObject.transform.rotation.x = glm::clamp(gameObject.transform.rotation.x, -1.5f, 1.5f);
    gameObject.transform.rotation.y = glm::mod(gameObject.transform.rotation.y, glm::two_pi<float>());

    float yaw = gameObject.transform.rotation.y;
    const glm::vec3 forwardDir{glm::sin(yaw), 0.0f, glm::cos(yaw)};
    const glm::vec3 rightDir{forwardDir.z, 0.0f, -forwardDir.x};
    const glm::vec3 upDir{0.0f, -1.0f, 0.0f};

    glm::vec3 moveDir{0.0f};

    if (keystate[keys.moveForward]) moveDir += forwardDir;
    if (keystate[keys.moveBackward]) moveDir -= forwardDir;
    if (keystate[keys.moveRight]) moveDir += rightDir;
    if (keystate[keys.moveLeft]) moveDir -= rightDir;
    if (keystate[keys.moveUp]) moveDir += upDir;
    if (keystate[keys.moveDown]) moveDir -= upDir;

    if (glm::dot(moveDir, moveDir) > std::numeric_limits<float>::epsilon()) {
        gameObject.transform.translation += moveSpeed * dt * glm::normalize(moveDir);
    }
}
}   // namespace pve