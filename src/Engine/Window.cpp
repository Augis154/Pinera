#include "Window.hpp"

// std headers
#include <stdexcept>

namespace pve {

PveWindow::PveWindow(std::string name) : windowName{name} {
    initWindow();
}

PveWindow::~PveWindow() {
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void PveWindow::initWindow() {
    SDL_Init(SDL_INIT_EVERYTHING);

    SDL_DisplayMode displayMode;
    SDL_GetCurrentDisplayMode(0, &displayMode);
    width = displayMode.w;
    height = displayMode.h;

    window = SDL_CreateWindow(windowName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height,
        SDL_WINDOW_VULKAN | SDL_WINDOW_FULLSCREEN | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_RESIZABLE);
}

void PveWindow::createWindowSurface(VkInstance instance, VkSurfaceKHR *surface) {
    if (SDL_Vulkan_CreateSurface(window, static_cast<VkInstance>(instance), reinterpret_cast<VkSurfaceKHR*>(surface)) != SDL_TRUE) {
        throw std::runtime_error("Failed to create window surface");
    }
}

void PveWindow::framebufferResizeCallback(int width, int height) {
    this->framebufferResized = true;
    this->width = width;
    this->height = height;
}
}   // namespace pve