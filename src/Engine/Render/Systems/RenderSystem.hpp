#pragma once

#include "../../Graphics/Pipeline.hpp"
#include "../../Device/Device.hpp"
#include "../../Object/Object.hpp"
#include "../../UserIO/Camera.hpp"
#include "../FrameInfo.hpp"

// libs
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

// std headers
#include <memory>
#include <vector>

namespace pve {

class RenderSystem {
public:
    RenderSystem(PveDevice& device, VkRenderPass renderPass, VkDescriptorSetLayout globalSetLayout);
    ~RenderSystem();

    RenderSystem(const RenderSystem &) = delete;
    RenderSystem &operator=(const RenderSystem &) = delete;

    void renderObjects(FrameInfo& frameInfo);

private:
    void createPipelineLayout(VkDescriptorSetLayout globalSetLayout);
    void createPipeline(VkRenderPass renderPass);

    PveDevice& pveDevice;

    std::unique_ptr<PvePipeline> pvePipeline;
    VkPipelineLayout pipelineLayout;
};
}   // namespace pve