#pragma once

#include "../Window.hpp"
#include "../Device/Device.hpp"
#include "../Graphics/SwapChain.hpp"

// std headers
#include <memory>
#include <vector>
#include <cassert>

namespace pve {

class PveRenderer {
public:
    PveRenderer(PveWindow& window, PveDevice& device);
    ~PveRenderer();

    PveRenderer(const PveRenderer &) = delete;
    PveRenderer &operator=(const PveRenderer &) = delete;

    bool isFrameInProgress() const { return isFrameStarted; }
    VkRenderPass getSwapChainRenderPass() const { return pveSwapChain->getRenderPass(); }
    float getAspectRatio() const { return pveSwapChain->extentAspectRatio(); }

    VkCommandBuffer getCurrentCommandBuffer() const {
        assert(isFrameStarted && "Cannot get command buffer when frame not in progress");
        return commandBuffers[currentFrameIndex];
    }

    int getFrameIndex() const {
        assert(isFrameStarted && "Cannot get frame index when frame not in progress");
        return currentFrameIndex;
    }

    VkCommandBuffer beginFrame();
    void endFrame();
    void beginSwapChainRenderPass(VkCommandBuffer commandBuffer);
    void endSwapChainRenderPass(VkCommandBuffer commandBuffer);

private:
    void createCommandBuffers();
    void freeCommandBuffers();
    void recreateSwapChain();

    PveWindow& pveWindow;
    PveDevice& pveDevice;
    std::unique_ptr<PveSwapChain> pveSwapChain;
    std::vector<VkCommandBuffer> commandBuffers;

    uint32_t currentImageIndex;
    int currentFrameIndex = 0;
    bool isFrameStarted = false;
};
}   // namespace pve