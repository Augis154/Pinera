#pragma once

#include "../UserIO/Camera.hpp"
#include "../Object/Buffer.hpp"

// lib
#include <vulkan/vulkan.hpp>

namespace pve {

#define MAX_LIGHTS 10

struct PointLight {
    glm::vec4 position{};   // ignore w
    glm::vec4 color{};  // w is intensity
};

// Global uniform buffer object
struct GlobalUbo {
    glm::mat4 projection{1.0f};
    glm::mat4 view{1.0f};
    glm::vec4 ambientLightColor{1.0f, 1.0f, 1.0f, 0.02f};   // w is intensity
    PointLight pointLights[MAX_LIGHTS];
    int lightsActive;
};

struct FrameInfo {
    int frameIndex;
    float frameTime;
    VkCommandBuffer commandBuffer;
    PveCamera& camera;
    VkDescriptorSet globalDescriptorSet;
    PveObject::Map &objects;
};

}   // namespace pve