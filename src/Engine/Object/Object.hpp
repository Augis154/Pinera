#pragma once

#include "Model.hpp"

// libs
#include <glm/gtc/matrix_transform.hpp>

// std headers
#include <memory>
#include <unordered_map>

namespace pve {

struct TransformComponent {
    glm::vec3 translation{}; // position offset
    glm::vec3 scale{1.0f, 1.0f, 1.0f};
    glm::vec3 rotation;

    // Matrix corrsponds to Translate * Ry * Rx * Rz * Scale
    // Rotations correspond to Tait-bryan angles of Y(1), X(2), Z(3)
    glm::mat4 mat4();
    glm::mat3 normalMatrix();
};

struct PointLightComponent {
    float lightIntensity = 1.0f;
};

class PveObject {
public:
    using id_t = unsigned int;
    using Map = std::unordered_map<id_t, PveObject>;

    static PveObject createObject() {
        static id_t currentId = 0;
        return PveObject{currentId++};
    }

    static PveObject makePointLight(float intensity = 5.0f, float radius = 0.1f, glm::vec3 color = glm::vec3(1.0f));

    PveObject(const PveObject&) = delete;
    PveObject &operator=(const PveObject&) = delete;
    PveObject(PveObject&&) = default;
    PveObject &operator=(PveObject&&) = default;

    id_t getId() { return id; }

    glm::vec3 color{};
    TransformComponent transform{};

    // Optional pointer components
    std::shared_ptr<PveModel> model{};
    std::unique_ptr<PointLightComponent> pointLight = nullptr;

private:
    PveObject(id_t objId) : id(objId) {}
    id_t id;
};
}   // namespace pve