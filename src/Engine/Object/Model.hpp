#pragma once

#include "../Device/Device.hpp"
#include "Buffer.hpp"

// libs
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>

// std headers
#include <vector>
#include <memory>

namespace pve {

class PveModel {
public:

    struct Vertex {
        glm::vec3 position{};
        glm::vec3 color{};
        glm::vec3 normal{};
        glm::vec2 uv{};

        static std::vector<VkVertexInputBindingDescription> getBindingDescriptions();
        static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions();

        bool operator==(const Vertex& other) const {
            return position == other.position &&
                   color == other.color &&
                   normal == other.normal &&
                   uv == other.uv;
        }
    };

    struct ModelBuilder {
        std::vector<Vertex> vertices{};
        std::vector<uint32_t> indices{};

        void loadModel(const std::string& filepath);
    };

    PveModel(PveDevice& device, const PveModel::ModelBuilder& builder);
    ~PveModel();

    PveModel(const PveModel &) = delete;
    PveModel &operator=(const PveModel &) = delete;

    static std::unique_ptr<PveModel> createModelFromFile(
        PveDevice& device, const std::string &filepath);

    void bind(VkCommandBuffer commandBuffer);
    void draw(VkCommandBuffer commandBuffer);

private:
    void createVertexBuffers(const std::vector<Vertex> &vertices);
    void createIndexBuffers(const std::vector<uint32_t> &indices);

    PveDevice& pveDevice;

    std::unique_ptr<PveBuffer> vertexBuffer;
    uint32_t vertexCount;

    bool hasIndexBuffer = false;
    std::unique_ptr<PveBuffer> indexBuffer;
    uint32_t indexCount;
};
}   // namespace pve