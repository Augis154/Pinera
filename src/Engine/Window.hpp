#pragma once

#include <vulkan/vulkan.hpp>
#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

// std headers
#include <string>

namespace pve {

class PveWindow {
public:
    PveWindow(std::string name);
    ~PveWindow();

    PveWindow(const PveWindow &) = delete;
    PveWindow &operator=(const PveWindow &) = delete;

    VkExtent2D getExtent() { return {static_cast<uint32_t>(width), static_cast<uint32_t>(height)}; }
    bool wasWindowResized() { return framebufferResized; }
    void resetWindowResizedFlag() { framebufferResized = false; }
    SDL_Window* getSDLwindow() const { return window; }

    void createWindowSurface(VkInstance instance, VkSurfaceKHR* surface);
    void framebufferResizeCallback(int width, int height);

private:
    void initWindow();

    int width;
    int height;
    bool framebufferResized = false;

    std::string windowName;
    SDL_Window* window;
};
}   // namespace pve