#pragma once

#include "Engine/Window.hpp"
#include "Engine/Device/Device.hpp"
#include "Engine/Object/Object.hpp"
#include "Engine/Render/Renderer.hpp"
#include "Engine/Descriptors.hpp"

// std headers
#include <memory>
#include <vector>

namespace pve {

class App {
public:

    App();
    ~App();

    App(const App&) = delete;
    App& operator=(const App&) = delete;

    void run();

private:
    void loadObjects();
    void handleSdlEvent(SDL_Event* event);

    PveWindow pveWindow{"Pinera Vulkan Engine"};
    PveDevice pveDevice{pveWindow};
    PveRenderer pveRenderer{pveWindow, pveDevice};

    // Order of declarations matter
    std::unique_ptr<PveDescriptorPool> globalPool{};
    PveObject::Map gameObjects;

    bool shouldClose = false;
};
}   // namespace pve