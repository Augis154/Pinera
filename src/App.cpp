#include "App.hpp"

#include "Engine/Render/Systems/RenderSystem.hpp"
#include "Engine/UserIO/Camera.hpp"
#include "Engine/UserIO/MovementController.hpp"
#include "Engine/Object/Buffer.hpp"
#include "Engine/Render/Systems/PointLightSystem.hpp"

// libs
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

// std headers
#include <stdexcept>
#include <array>
#include <cassert>
#include <chrono>

namespace pve {

App::App() {
    globalPool = PveDescriptorPool::Builder(pveDevice)
        .setMaxSets(PveSwapChain::MAX_FRAMES_IN_FLIGHT)
        .addPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, PveSwapChain::MAX_FRAMES_IN_FLIGHT)
        .build();

    loadObjects();
}

App::~App() {}

void App::run() {
    std::vector<std::unique_ptr<PveBuffer>> uboBuffers(PveSwapChain::MAX_FRAMES_IN_FLIGHT);

    for (size_t i = 0; i < uboBuffers.size(); i++) {
        uboBuffers[i] = std::make_unique<PveBuffer>(
            pveDevice,
            sizeof(GlobalUbo),
            1,
            VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
            // VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT || VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
            pveDevice.properties.limits.minUniformBufferOffsetAlignment);
        uboBuffers[i]->map();
    }

    auto globalSetLayout = PveDescriptorSetLayout::Builder(pveDevice)
        .addBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_ALL_GRAPHICS)
        .build();

    std::vector<VkDescriptorSet> globalDescriptorSets(PveSwapChain::MAX_FRAMES_IN_FLIGHT);
    for (size_t i = 0; i < globalDescriptorSets.size(); i++) {
        auto bufferInfo = uboBuffers[i]->descriptorInfo();
        PveDescriptorWriter(*globalSetLayout, *globalPool)
            .writeBuffer(0, &bufferInfo)
            .build(globalDescriptorSets[i]);
    }

    RenderSystem renderSystem{
        pveDevice,
        pveRenderer.getSwapChainRenderPass(),
        globalSetLayout->getDescriptorSetLayout()};

    PointLightSystem pointLightSystem{
        pveDevice,
        pveRenderer.getSwapChainRenderPass(),
        globalSetLayout->getDescriptorSetLayout()};

    PveCamera camera{};

    camera.setViewTarget(glm::vec3(-1.0f, -2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 2.5f));

    auto viewerObject = PveObject::createObject();
    MovementController controller{};

    auto currentTime = std::chrono::high_resolution_clock::now();
    SDL_Event event;

    // float moveValue = 0.0f;
    // float accValue = 0.005f;
    // bool dir = 0;

    while (!shouldClose) {
        while(SDL_PollEvent(&event)) handleSdlEvent(&event);

        auto newTime = std::chrono::high_resolution_clock::now();
        float frameTime = std::chrono::duration<float, std::chrono::seconds::period>(newTime - currentTime).count();
        currentTime = newTime;

        // accValue *= 1.001f;
        // moveValue = dir ? moveValue - accValue : moveValue + accValue;
        // if (moveValue > 5 && dir == 0) {dir = 1; accValue = 0.005f;}
        // else if (moveValue < -5 && dir == 1) {dir = 0; accValue = 0.005f;}
        
        // gameObjects.find(1)->second.transform.translation = {0.5f, -0.5f, moveValue};

        controller.moveInPlaneXZ(frameTime, viewerObject);
        camera.setViewYXZ(viewerObject.transform.translation, viewerObject.transform.rotation);

        float aspect = pveRenderer.getAspectRatio();
        // camera.setOrthographicProjection(-aspect, aspect, -1, 1, -1, 1);
        camera.setPerspectiveProjection(glm::radians(70.0f), aspect, 0.05f, 100.0f);
        
        if (auto commandBuffer = pveRenderer.beginFrame()) {
            int frameIndex = pveRenderer.getFrameIndex();
            FrameInfo frameInfo {
                frameIndex,
                frameTime,
                commandBuffer,
                camera,
                globalDescriptorSets[frameIndex],
                gameObjects
            };

            // Update objects and memory
            GlobalUbo ubo{};
            ubo.projection = camera.getProjection();
            ubo.view = camera.getView();
            pointLightSystem.update(frameInfo, ubo);
            uboBuffers[frameIndex]->writeToBuffer(&ubo);
            uboBuffers[frameIndex]->flush();

            // Render generated frame
            pveRenderer.beginSwapChainRenderPass(commandBuffer);
            renderSystem.renderObjects(frameInfo);
            pointLightSystem.render(frameInfo);
            pveRenderer.endSwapChainRenderPass(commandBuffer);
            pveRenderer.endFrame();
        }
    }

    vkDeviceWaitIdle(pveDevice.device());
}

void App::loadObjects() {
    std::shared_ptr<PveModel> pveModel{};

    pveModel = PveModel::createModelFromFile(pveDevice, "Assets/Models/FlatVase.obj");
    auto flatVase = PveObject::createObject();
    flatVase.model = pveModel;
    flatVase.transform.translation = {-0.5f, -0.5f, 0.0f};
    flatVase.transform.scale = glm::vec3(2.5f);
    gameObjects.emplace(flatVase.getId(), std::move(flatVase));

    pveModel = PveModel::createModelFromFile(pveDevice, "Assets/Models/SmoothVase.obj");
    auto smoothVase = PveObject::createObject();
    smoothVase.model = pveModel;
    smoothVase.transform.translation = {0.5f, -0.5f, 0.0f};
    smoothVase.transform.scale = glm::vec3(2.5f);
    gameObjects.emplace(smoothVase.getId(), std::move(smoothVase));

    pveModel = PveModel::createModelFromFile(pveDevice, "Assets/Models/Quad.obj");
    auto floor = PveObject::createObject();
    floor.model = pveModel;
    floor.transform.translation = {0.0f, 0.5f, 0.0f};
    floor.transform.scale = {7.0f, 1.0f, 5.0f};
    gameObjects.emplace(floor.getId(), std::move(floor));

    // pveModel = PveModel::createModelFromFile(pveDevice, "Models/Cube.obj");
    // auto cube = PveObject::createObject();
    // cube.model = pveModel;
    // cube.transform.translation = {0.0f, 0.5f, 1.0f};
    // cube.transform.scale = {10.0f, 0.001f, 5.0f};
    // gameObjects.emplace(std::move(cube));

    // pveModel = PveModel::createModelFromFile(pveDevice, "Assets/Models/ak47.obj");
    // auto cube = PveObject::createObject();
    // cube.model = pveModel;
    // cube.transform.translation = {0.0f, 0.0f, 1.0f};
    // cube.transform.scale = glm::vec3(-0.03f);
    // gameObjects.emplace(cube.getId(), std::move(cube));

    // pveModel = PveModel::createModelFromFile(pveDevice, "Assets/Models/RaceCar/Car.obj");
    // auto cube = PveObject::createObject();
    // cube.model = pveModel;
    // cube.transform.rotation = {-1.5f, 0.0f, 0.0f};
    // cube.transform.translation = {0.0f, 0.0f, 1.0f};
    // cube.transform.scale = glm::vec3(-0.03f);
    // gameObjects.emplace(cube.getId(), std::move(cube));

    // {
    //     auto pointLight = PveObject::makePointLight();
    //     gameObjects.emplace(pointLight.getId(), std::move(pointLight));
    // }

    std::vector<glm::vec3> lightColors {
        {1.0f, 0.1f, 0.1f},
        {0.1f, 0.1f, 1.0f},
        {0.1f, 1.0f, 0.1f},
        {1.0f, 1.0f, 0.1f},
        {0.1f, 1.0f, 1.0f},
        {1.0f, 1.0f, 1.0f}
    };

    for (size_t i = 0; i < lightColors.size(); i++) {
        auto pointLight = PveObject::makePointLight(0.5f);
        pointLight.color = lightColors[i];
        auto rotateLight = glm::rotate(
            glm::mat4(1.0f),
            (i * glm::two_pi<float>()) / lightColors.size(),
            {0.0f, -1.0f, 0.0f});
        pointLight.transform.translation = glm::vec3(rotateLight * glm::vec4(-1.0f, -1.0f, -1.0f, 1.0f));
        gameObjects.emplace(pointLight.getId(), std::move(pointLight));
    }
}

void App::handleSdlEvent(SDL_Event* event) {
    switch (event->type) {
        case SDL_WINDOWEVENT:
            switch(event->window.event) {
                case SDL_WINDOWEVENT_RESIZED:
                    pveWindow.framebufferResizeCallback(pveWindow.getExtent().width, pveWindow.getExtent().height);
                    break;

                case SDL_WINDOWEVENT_CLOSE:
                    shouldClose = true;
                    break;
            }
            break;
        
        case SDL_QUIT:
            shouldClose = true;
            break;
    }
}
}   // namespace pve