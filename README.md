# Project Pinera
3D engine using Vulkan API.

Based on [this](https://www.youtube.com/watch?v=Y9U9IE0gVHA&list=PL8327DO66nu9qYVKLDmdLW_84-yE4auCR "
Brendan Galea") youtube series.

Started because I had nothing to do:)

## Dependencies:
* [SDL2](https://github.com/libsdl-org/SDL)

* [GLM](https://github.com/g-truc/glm)

* [tinyobjloader](https://github.com/tinyobjloader/tinyobjloader)

## To compile and run (only tested on linux):
* clone this repository

```
cd Pinera
./buildUnix.sh
```
